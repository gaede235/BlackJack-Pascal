package Blackjack;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

class Card {
    private final String rank;
    private final String suit;

    public Card(String rank, String suit) {
        this.rank = rank;
        this.suit = suit;
    }

    public String toString() {
        return rank + " von " + suit;
    }

    public int getValue() {
        switch (rank) {
            case "Ass":
                return 11;
            case "2", "3", "4", "5", "6", "7", "8", "9", "10":
                return Integer.parseInt(rank);
            default:
                return 10;
        }
    }
}

class Deck {
    private final List<Card> cards;

    public Deck() {
        cards = new ArrayList<>();
        String[] suits = {"Herz", "Karo", "Kreuz", "Pik"};
        String[] ranks = {"2", "3", "4", "5", "6", "7", "8", "9", "10", "Bube", "Dame", "König", "Ass"};

        for (String suit : suits) {
            for (String rank : ranks) {
                cards.add(new Card(rank, suit));
            }
        }
    }

    public void shuffle() {
        Random random = new Random();
        for (int i = cards.size() - 1; i > 0; i--) {
            int index = random.nextInt(i + 1);
            Card temp = cards.get(index);
            cards.set(index, cards.get(i));
            cards.set(i, temp);
        }
    }

    public Card drawCard() {
        return cards.remove(cards.size() - 1);
    }
}

class Hand {
    private final List<Card> cards;

    public Hand() {
        cards = new ArrayList<>();
    }

    public void addCard(Card card) {
        cards.add(card);
    }

    public int calculateHandValue() {
        int totalValue = 0;
        int aceCount = 0;

        for (Card card : cards) {
            totalValue += card.getValue();
            if (card.getValue() == 11) {
                aceCount++;
            }
        }

        while (totalValue > 21 && aceCount > 0) {
            totalValue -= 10;
            aceCount--;
        }

        return totalValue;
    }

    public void displayHand() {
        for (Card card : cards) {
            System.out.println(card);
        }
        System.out.println("Endergebnis: " + calculateHandValue());
    }
}

public class Blackjack {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Deck deck = new Deck();
        deck.shuffle();

        Hand playerHand = new Hand();
        Hand dealerHand = new Hand();

        playerHand.addCard(deck.drawCard());
        playerHand.addCard(deck.drawCard());
        dealerHand.addCard(deck.drawCard());
        dealerHand.addCard(deck.drawCard());

        System.out.println("Meine Karten:");
        playerHand.displayHand();

        while (playerHand.calculateHandValue() <= 21) {
            System.out.println("1. Karte ziehen  2. Bleiben");
            int choice = scanner.nextInt();

            if (choice == 1) {
                playerHand.addCard(deck.drawCard());
                System.out.println("Meine Karten:");
                playerHand.displayHand();
            } else if (choice == 2) {
                break;
            }
        }

        System.out.println("Dealer Karten:");
        dealerHand.displayHand();

        while (dealerHand.calculateHandValue() < 17) {
            dealerHand.addCard(deck.drawCard());
            System.out.println("Dealer zieht eine Karte:");
            dealerHand.displayHand();
        }

        int playerTotal = playerHand.calculateHandValue();
        int dealerTotal = dealerHand.calculateHandValue();

        if (playerTotal > 21) {
            System.out.println("Du hast Verloren! Dealer hat gewonnen.");
        } else if (dealerTotal > 21) {
            System.out.println("Dealer hat Verloren! Du hast gewonnen.");
        } else if (playerTotal > dealerTotal) {
            System.out.println("Du hast gewonnen!");
        } else if (playerTotal < dealerTotal) {
            System.out.println("Dealer hat gewonnen!");
        } else {
            System.out.println("Unentschieden!");
        }

        scanner.close();
    }
}
